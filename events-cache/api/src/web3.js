const Web3 = require('web3');

let web3;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  web3 = new Web3(window.web3.currentProvider);
} else {
  //const provider = new Web3.providers.HttpProvider('http://45.150.187.29:8545');
  const provider = new Web3.providers.HttpProvider('http://45.150.187.30:8545');
  //const provider = new Web3.providers.WebsocketProvider('http://45.150.187.30:8546');
  //const provider = new Web3.providers.HttpProvider('http://127.0.0.1:8545');
  web3 = new Web3(provider);
}

module.exports = web3;