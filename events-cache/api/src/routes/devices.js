const express = require("express")
const ethers = require("ethers")
const { BadRequest, NotFound ,Forbidden} = require("../utils/errors")
const { queryDevice, queryAll } = require("../utils/dbqueries")
const e = require("express")

const web3 = require("../web3.js")
const DeviceFactory = require('../blockchain/abi/DeviceFactory.json')

const app = express()


const DEVICEFACTORY_ADDRESS = process.env.DEVICEFACTORY_ADDRESS
const STAMPPROOFS_ADDRESS = process.env.STAMPPROOFS_ADDRESS
const privateKey = process.env.PRIVATE_KEY
const publicKey = process.env.PUBLIC_KEY

const deviceFactoryIface = new ethers.utils.Interface(
  require("../blockchain/abi/DeviceFactory.json").abi
)
const depositDeviceIface = new ethers.utils.Interface(
  require("../blockchain/abi/DepositDevice.json").abi
)
const stampProofsIface = new ethers.utils.Interface(
  require("../blockchain/abi/StampProofs.json").abi
)
const provider = new ethers.providers.JsonRpcProvider(
  process.env.BLOCKCHAIN_ENDPOINT
)
// const provider = new ethers.providers.WebSocketProvider(
//   process.env.BLOCKCHAIN_ENDPOINT
// )
//await provider.getNetwork()
const signer = new ethers.Wallet( privateKey, provider)
const deviceFactoryContract = new ethers.Contract(
  DEVICEFACTORY_ADDRESS,
  require("../blockchain/abi/DeviceFactory.json").abi,
  signer
)
var nonce
signer.getTransactionCount().then(n => {
  nonce = n
})


dfContract = new web3.eth.Contract(DeviceFactory.abi, DEVICEFACTORY_ADDRESS)


app.get("/:deviceAddress", async (req, res, next) => {
  try {
    const deviceAddress = req.params.deviceAddress

    if (!ethers.utils.isAddress(deviceAddress)) {
      throw new BadRequest("Wrong address")
    }

    const device = await queryDevice(deviceAddress)

    if (!device) throw new NotFound("Device not found")

    return res.json({
      status: "success",
      data: {
        device,
      },
    })
  } catch (e) {
    next(e)
  }
})

app.get("/", async (req, res, next) => {
  try {
    const all = await queryAll()
    if (!all) throw new NotFound("Devices not found")
    return res.json({
      status: "success",
      data: { all },
    })
  } catch (e) {
    next(e)
  }
})

app.post("/create", async (req, res, next) => {
  const uid = req.body.uid;
  const initValue = parseInt(req.body.initValue);
  const owner = req.body.owner;
  try{
    if (isNaN(initValue)){
      throw new BadRequest("initValue is not a number")
    }

    console.log(`nonce: ${nonce}`)
    const n = nonce++
    console.log(`n: ${n}`)
    const txResponse = await deviceFactoryContract.createDevice(uid,initValue,owner, {gasLimit:8000000,nonce:n})
    const txReceipt = await txResponse.wait()

    var args;
    txReceipt.events.forEach(log => {
      if (log.event == 'DeviceCreated'){
        args = deviceFactoryIface.parseLog(log).args
      }
    })
    console.log("Args:")
    console.log(args)

    res.json({
      status: "success",
      data: {
        deviceAddress: args._deviceAddress
      },
    })

  }
  catch (e) {
    next(e)
  }
})


app.post("/transfer", async (req, res, next) => {
  const deposit = parseInt(req.body.deposit);
  const new_owner = req.body.new_owner;
  const new_registrant = req.body.new_registrant;
  const device_address = req.body.device_address;
  try{
    if (isNaN(deposit)){
      throw new BadRequest("deposit is not a number")
    }
    if (!ethers.utils.isAddress(new_owner)) {
      throw new BadRequest("Bad new owner address")
    }
    if (!ethers.utils.isAddress(device_address)) {
      throw new BadRequest("Bad device address")
    }

    const depositDeviceContract = new ethers.Contract(
      device_address,
      require("../blockchain/abi/DepositDevice.json").abi,
      signer
    )
    console.log(`nonce: ${nonce}`)
    const n = nonce++
    console.log(`n: ${n}`)
    const txResponse = await depositDeviceContract.transferDevice(new_owner,deposit,new_registrant,{gasLimit:8000000,nonce:n})
    const txReceipt = await txResponse.wait()

    var args;
    txReceipt.events.forEach(log => {
      if (log.event == 'DeviceTransferred'){
        args = depositDeviceIface.parseLog(log).args
      }
    })
    console.log("Args:")
    console.log(args)


    res.json({
      status: "success",
      data: {
        deviceAddress: args.deviceAddress,
        new_owner: args.new_owner,
        new_registrant: args.new_registrant
      },
    })

  }
  catch (e) {
    next(e)
  }
})

// types = [
//   "ProofDataWipe",
//   "ProofFunction",
//   "ProofTransfer",
//   "ProofReuse",
//   "ProofRecycling"
// ];
app.post("/generateProof", async (req, res, next) => {
  const proof_type = req.body.proof_type;
  const device_address = req.body.device_address;
  try{
    if (!ethers.utils.isAddress(device_address)) {
      throw new BadRequest("Bad device address")
    }


    const depositDeviceContract = new ethers.Contract(
      device_address,
      require("../blockchain/abi/DepositDevice.json").abi,
      signer
    )
    
    var txResponse
    var txReceipt
    var proofAuthor
    var date
    var price
    var n

    switch(proof_type){
      case "ProofDataWipe":
        const erasureType = req.body.erasureType
        date = req.body.date
        const erasureResult = req.body.erasureResult == 'true';
        proofAuthor = req.body.proofAuthor
        if (!ethers.utils.isAddress(proofAuthor)) {
          throw new BadRequest("Bad proofAuthor address")
        }
        console.log(`nonce: ${nonce}`)
        n = nonce++
        console.log(`n: ${n}`)
        txResponse = await depositDeviceContract.generateDataWipeProof(erasureType,date,erasureResult,proofAuthor,{gasLimit:8000000,nonce:n})
        txReceipt = await txResponse.wait()
        var args;
        txReceipt.events.forEach(log => {
          if (log.event == 'dataWipeProof') {
            args = depositDeviceIface.parseLog(log).args
          }
        })
        console.log("Args:")
        console.log(args)
        res.json({
          status: "success",
          data: {
            userAddress: args.userAddress,
            deviceAddress: args.deviceAddress,
            erasureType: args.erasureType,
            erasureResult: args.erasureResult
          },
        })
        break
      case "ProofFunction":
        // uint256 score,
        // uint256 diskUsage,
        // string algorithmVersion,
        // address proofAuthor
        const score = parseInt(req.body.score)
        const diskUsage = parseInt(req.body.diskUsage)
        const algorithmVersion = req.body.algorithmVersion
        proofAuthor = req.body.proofAuthor
        if (!ethers.utils.isAddress(proofAuthor)) {
          throw new BadRequest("Bad proofAuthor address")
        }
        if (isNaN(score)){
          throw new BadRequest("score is not a number")
        }
        if (isNaN(diskUsage)){
          throw new BadRequest("diskUsage is not a number")
        }
        console.log(`nonce: ${nonce}`)
        n = nonce++
        console.log(`n: ${n}`)
        txResponse = await depositDeviceContract.generateFunctionProof(score,diskUsage,algorithmVersion,proofAuthor,{gasLimit:8000000,nonce:n})
        txReceipt = await txResponse.wait()
        var args;
        txReceipt.events.forEach(log => {
          if (log.event == 'functionProof') {
            args = depositDeviceIface.parseLog(log).args
          }
        })
        console.log("Args:")
        console.log(args)
        res.json({
          status: "success",
          data: {
            userAddress: args.userAddress,
            deviceAddress: args.deviceAddress,
            score: args.score,
            diskUsage: args.diskUsage,
            algorithmVersion: args.algorithmVersion
          },
        })
        break
      case "ProofTransfer":
        // address supplier,
        // address receiver,
        // uint256 deposit,
        // bool isWaste
        const supplier = req.body.supplier
        const receiver = req.body.receiver
        const deposit = parseInt(req.body.deposit)
        const isWaste = req.body.isWaste == 'true';
        if (isNaN(deposit)){
          throw new BadRequest("deposit is not a number")
        }
        if (!ethers.utils.isAddress(supplier)) {
          throw new BadRequest("Bad supplier address")
        }
        if (!ethers.utils.isAddress(receiver)) {
          throw new BadRequest("Bad receiver address")
        }
        console.log(`nonce: ${nonce}`)
        n = nonce++
        console.log(`n: ${n}`)
        txResponse = await depositDeviceContract.generateTransferProof(supplier,receiver,deposit,isWaste,{gasLimit:8000000,nonce:n})
        txReceipt = await txResponse.wait()
        var args;
        txReceipt.events.forEach(log => {
          if (log.event == 'transferProof') {
            args = depositDeviceIface.parseLog(log).args
          }
        })
        console.log("Args:")
        console.log(args)
        res.json({
          status: "success",
          data: {
            supplierAddress: args.supplierAddress,
            receiverAddress: args.receiverAddress,
            deviceAddress: args.deviceAddress
          },
        })
        break
      case "ProofReuse":
        // string receiverSegment,
        // string idReceipt,
        // uint256 price
        const receiverSegment = req.body.receiverSegment
        const idReceipt = req.body.idReceipt
        price = parseInt(req.body.price)
        if (isNaN(price)){
          throw new BadRequest("price is not a number")
        }
        console.log(`nonce: ${nonce}`)
        n = nonce++
        console.log(`n: ${n}`)
        txResponse = await depositDeviceContract.generateReuseProof(receiverSegment,idReceipt,price, {gasLimit:8000000,nonce:n})
        txReceipt = await txResponse.wait()
        var args;
        txReceipt.events.forEach(log => {
          if (log.event == 'reuseProof') {
            args = depositDeviceIface.parseLog(log).args
          }
        })
        console.log("Args:")
        console.log(args)
        res.json({
          status: "success",
          data: {
            userAddress: args.userAddress,
            deviceAddress: args.deviceAddress,
            receiverSegment: args.receiverSegment,
            idReceipt: args.idReceipt,
            price: args.price
          },
        })
        break
      case "ProofRecycling":
        // string collectionPoint,
        // string date,
        // string contact,
        // string ticket,
        // string gpsLocation,
        // string recyclerCode
        const collectionPoint = req.body.collectionPoint
        date = req.body.date
        const contact = req.body.contact
        const ticket = req.body.ticket
        const gpsLocation = req.body.gpsLocation
        const recyclerCode = req.body.recyclerCode
        console.log(`nonce: ${nonce}`)
        n = nonce++
        console.log(`n: ${n}`)
        txResponse = await depositDeviceContract.generateReuseProof(collectionPoint,date,contact,ticket,gpsLocation,recyclerCode,{gasLimit:8000000,nonce:n})
        txReceipt = await txResponse.wait()
        var args;
        txReceipt.events.forEach(log => {
          if (log.event == 'recycleProof') {
            args = depositDeviceIface.parseLog(log).args
          }
        })
        console.log("Args:")
        console.log(args)
        res.json({
          status: "success",
          data: {
            userAddress: args.userAddress,
            deviceAddress: args.deviceAddress,
            date: args.date
          },
        })
        break
      default:
        throw new BadRequest("Incorrect proof type")
    }

    

  }
  catch (e) {
    next(e)
  }
})

app.post("/recycle", async (req, res, next) => {
  const device_address = req.body.device_address;
  try{
    if (!ethers.utils.isAddress(device_address)) {
      throw new BadRequest("Bad device address")
    }

    const depositDeviceContract = new ethers.Contract(
      device_address,
      require("../blockchain/abi/DepositDevice.json").abi,
      signer
    )
    console.log(`nonce: ${nonce}`)
    const n = nonce++
    console.log(`n: ${n}`)
    const txResponse = await depositDeviceContract.recycle({gasLimit:8000000,nonce:n})
    const txReceipt = await txResponse.wait()

    var args;
    txReceipt.events.forEach(log => {
      if (log.event == 'DeviceRecycled'){
        args = depositDeviceIface.parseLog(log).args
      }
    })
    console.log("Args:")
    console.log(args)


    res.json({
      status: "success",
      data: {
        deviceAddress: args.deviceAddress
      },
    })

  }
  catch (e) {
    next(e)
  }
})


const isValidSHA3 = (hash) => {
  if (typeof hash !== "string") return false
  return hash.match("^[a-fA-F0-9]{64}$")
}


app.post("/createStamp", async (req, res, next) => {
  const hash = req.body.hash;
  try{
    if (!isValidSHA3(hash)) throw new BadRequest("Invalid Hash")

    const logs = await provider.getLogs({
      fromBlock: 0,
      topics: [
        ethers.utils.id("stampProof(bytes32,uint256)"),
        ethers.utils.hexZeroPad(`0x${hash}`),
      ],
    })

    if (logs.length > 0)
      throw new Forbidden("This document has already been stamped")

    const stampProofsContract = new ethers.Contract(
      STAMPPROOFS_ADDRESS,
      require("../blockchain/abi/StampProofs.json").abi,
      signer
    )

    console.log(`nonce: ${nonce}`)
    const n = nonce++
    console.log(`n: ${n}`)
    const txResponse = await stampProofsContract.setProof(`0x${hash}`,{gasLimit:8000000,nonce:n})
    const txReceipt = await txResponse.wait()

    var args;
    txReceipt.events.forEach(log => {
      if (log.event == 'stampProof'){
        args = stampProofsIface.parseLog(log).args
      }
    })
    console.log("Args:")
    console.log(args)


    res.json({
      status: "success",
      data: {
        hash: args.hash.substring(2),
        timestamp: args.timestamp.toNumber()
      },
    })

  }
  catch (e) {
    next(e)
  }
})

app.post("/checkStamp", async (req, res, next) => {
  const hash = req.body.hash;
  try {
    if (!isValidSHA3(hash)) throw new BadRequest("Invalid Hash")

    const logs = await provider.getLogs({
      fromBlock: 0,
      topics: [
        ethers.utils.id("stampProof(bytes32,uint256)"),
        ethers.utils.hexZeroPad(`0x${hash}`),
      ],
    })

    if (logs.length === 0)
      throw new NotFound("No stamps found for this document")

    stamps = logs.map((log) => {
      const event = stampProofsIface.parseLog(log).args
      return {
        hash: event.hash.substring(2),
        date: event.timestamp.toNumber(),
      }
    })

    res.json({
      status: "success",
      data: {
        stamps
      },
    })

  }
  catch (e) {
    next(e)
  }
})




module.exports = app
