# NGI eReuse-Ledger API

API for the testbed of the NGI-Atlantic project, on the deployment of a permissioned distributed ledger to support experimentation about device traceability.

The NGI eReuse-Ledger testbed offers an API and a service that can record events or transactions (we call them proofs) about devices (decentralized identifiers or DID), and run smart contracts to account for events that happen in the lifespan of a device for traceability and accountability, including an economic deposit mechanism to implement deposit-refund systems to automatically reward correct behaviors according to a contract.

## This respository

### [API and events listener](events-cache)
This directory contains the API code that interacts with the smart contracts on the Ethereum blockchain. It also contains a listener that records in a database the events relating to the creation of devices and generation of proofs.

### [WIP frontend](reports-platform)
This directory contains an experimental React frontend. It mainly allows the generation and checking of stamps of an uploaded file's hash.

## Other repositories
The code for the project spans several other repositories:

- [eReuse Smart Contracts](https://gitlab.com/dsg-upc/ereuse_smart_contracts): Core smart contracts of the project.
- [Token transfer system](https://gitlab.com/dsg-upc/e2e): An ERC20 token compliant smart contract system.
- [Ethereum node deployment](https://gitlab.com/dsg-upc/poa_blockchain_extranode): Deployment of a node to our testbed.
- [API tests](https://gitlab.com/dsg-upc/api_tests): Scripts to test the performance and correctness of the testbed.



